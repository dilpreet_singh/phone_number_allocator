class CreateUserAllocatedNumbers < ActiveRecord::Migration[5.2]
  def change
    create_table :user_allocated_numbers do |t|
      t.references :user
      t.string :phone_number
    end
    add_index :user_allocated_numbers, :phone_number
    add_index :user_allocated_numbers, [:user_id, :phone_number]
    add_foreign_key :user_allocated_numbers, :users, column: :user_id
  end
end
