class BaseSchema < Dry::Validation::Schema
  include Dry::Types.module
  configure do |config|
    config.input_processor = :form
    config.hash_type = :symbolized
    config.type_specs = true

    def call(input)
      options.fetch(:defaults, []).each do |key, value|
        input[key.to_sym] ||= value
      end
      super(input)
    end
  end

end
