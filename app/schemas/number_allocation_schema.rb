NumberAllocationSchema = Dry::Validation.Schema(BaseSchema) do
  optional(:phone_number).filled?
end
