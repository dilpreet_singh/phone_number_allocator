module Users
  LoginSchema = Dry::Validation.Schema(BaseSchema) do
    required(:email, :string).filled?(:str?)
    required(:password, :string).filled?(:str?)
  end
end
