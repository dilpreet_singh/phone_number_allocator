class NumberAllocationService
  include ::Utils::NumberFormatter
  attr_accessor :errors, :user_allocated_number

  def initialize(user, params)
    @user = user
    @params = params
    @errors = {}
  end

  def create
    @params[:phone_number] = assign_random_number unless @params[:phone_number]
    number = format_number(@params[:phone_number].to_s)
    return @errors.merge!(number: ['Invalid number requested']) unless number
    @user_allocated_number = @user.allocated_numbers.create(phone_number: number.to_s)
    @errors.merge!(@user_allocated_number.errors.messages) if @user_allocated_number.errors.any?
  end

  private

  def assign_random_number
    number = random_valid_number
    while UserAllocatedNumber.where(phone_number: number).exists?
      number = random_valid_number
    end
    number.to_s
  end

  def random_valid_number
    numbers = rand(111..999), rand(111..999), rand(1111..9999)
    number = numbers.join('')
    number
  end

end
