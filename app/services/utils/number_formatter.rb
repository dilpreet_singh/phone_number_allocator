module Utils
  module NumberFormatter

    NUMBER_SIZE = 10

    def format_number(number)
      return if number.size != 10
      number = number.gsub('-', '')
      splitted_numbers = number[0..2], number[3..5], number[6..9]
      splitted_numbers.all? { |num| valid_digit?(num, num.size) } ? number : nil
    end

    def valid_digit?(number, size)
      number.to_i >= ('1' * size).to_i && number.to_i <= ('9' * size).to_i
    end
  end
end
