module API
  module V1
    class SessionsController < ApplicationController

      skip_before_action :authenticate_user!

      def create
        @user = User.find_by_email(user_params[:email])
        if @user&.valid_password?(user_params[:password])
          token = JsonWebToken.encode(user_id: @user.id)
          render json: { token: token, name: @user.name }, status: :ok
        else
          render json: { error: 'unauthorized' }, status: :unauthorized
        end
      end

      private

      def user_params
        params.permit(:email, :password)
      end
    end
  end
end
