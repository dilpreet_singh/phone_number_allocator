module API
  module V1
    class NumberAllocationsController < ApplicationController

      def create
        service = ::NumberAllocationService.new(current_user, number_params)
        service.create
        if service.errors.any?
          render json: { errors: service.errors }, status: :unprocessable_entity
        else
          render json: { data: service.user_allocated_number.as_json(only: :phone_number) }, status: :ok
        end
      end

      private

      def number_params
        params.permit(:phone_number)
      end

    end
  end
end
