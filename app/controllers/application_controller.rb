class ApplicationController < ActionController::API

  before_action :authenticate_user!

  def current_user
    @current_user ||= fetch_current_user
  end

  def fetch_current_user
    header = request.headers['session-id']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebToken.decode(header)
      User.find(@decoded[:user_id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: 'Please Login first.' }, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: { errors: 'Please login first.' }, status: :unauthorized
    end
  end

  protected

  def authenticate_user!
    render json: { message: 'Unauthorized' }, status: :unauthorized unless current_user
  end

  def render_error_response(exception)
    render json: { errors: exception.errors }, status: :bad_request
  end
end
