# == Schema Information
#
# Table name: user_allocated_numbers
#
#  id           :bigint(8)        not null, primary key
#  user_id      :bigint(8)
#  phone_number :string(255)
#  special      :boolean          default(FALSE)
#

class UserAllocatedNumber < ApplicationRecord
  belongs_to :user

  validates :user_id, :phone_number, presence: true
  validates :phone_number, uniqueness: true
end
