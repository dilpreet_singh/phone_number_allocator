# USAGE

Create User from rails console

```ruby
  User.create(name: 'dilpreet', email: 'dilpreet@abc.com', password: 123456)
```

Login from API

```ruby
   POST '/api/v1/login'
   BODY { email: 'dilpreet@abc.com', password: 123456 }
```

Phone number allocation API - If phone number is not provided, it will randomly allocate a number

```ruby
   POST '/api/v1/number_allocations'
   BODY { phone_number: '1111111111' } # optional
```
